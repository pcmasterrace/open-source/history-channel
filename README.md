# History Channel

**History Channel** is a script that archives all public and private channels on a Slack server into a local or remote database. 

**NOTE**: At this time, this only fetches channels, users, and messages. This does not archive uploaded files, instant messages, or multiparty instant messages (MPIM, aka group chats). 

## Installation

This script requires Node.js 8+. [You can download an installer here](https://nodejs.org/en/) if you don't already have it installed on your machine.

After that, download or clone this repository locally, then install the required dependencies with Yarn: 

    yarn

or with NPM:

    npm install

You will also need to set up a database for this tool to archive into. I personally used an offline SQLite database; I've left an empty SQLite database file in the project root so Sequelize doesn't complain.

## Configuration

Configuration for History Channel is set via a file in the root of the project directory called `config.json`. To set it up: 

1. Duplicate or rename `config_sample.json` to `config.json`. 
2. Fill in your Slack API key in the Slack section. If you don't have one, [create a new app here](https://api.slack.com/apps) and get the API key from the "Install App" option in the sidebar.
3. If you're using a SQLite database, leave the `database` section as-is. If not, fill in the relevant details. This object gets passed directly into [Sequelize](http://docs.sequelizejs.com/); [all available options are located here](http://docs.sequelizejs.com/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor). If you have a URI, set the `database` option with that string and change the `dialect` option if needed. I recommend leaving logging off, as it just spits out every SQL query run to the console.

Once that is done, you can run the script with `yarn run`/`npm run`, and the script should begin doing its magic. 