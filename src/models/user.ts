import * as Sequelize from "sequelize";

export class UserModel {
    protected sql: Sequelize.Sequelize;
    protected models: Sequelize.ModelsHashInterface;
    public model: Sequelize.Model<any, any>;

    constructor (sql: Sequelize.Sequelize) {
        this.sql = sql;
        this.models = this.sql.models;
        this.defineModel();
    }

    private defineModel (): void {
        this.sql.define("user", {
            id: {
                type: Sequelize.STRING,
                primaryKey: true
            },
            team_id: Sequelize.STRING,
            name: Sequelize.STRING,
            deleted: Sequelize.BOOLEAN,
            color: Sequelize.STRING,
            real_name: Sequelize.STRING,
            tz: Sequelize.STRING,
            tz_label: Sequelize.STRING,
            tz_offset: Sequelize.INTEGER,
            profile_avatar_hash: Sequelize.STRING,
            profile_status_text: Sequelize.STRING,
            profile_status_emoji: Sequelize.STRING,
            profile_real_name: Sequelize.STRING,
            profile_display_name: Sequelize.STRING,
            profile_real_name_normalized: Sequelize.STRING,
            profile_display_name_normalized: Sequelize.STRING,
            profile_email: Sequelize.STRING,
            profile_image_24: Sequelize.STRING,
            profile_image_32: Sequelize.STRING,
            profile_image_48: Sequelize.STRING,
            profile_image_72: Sequelize.STRING,
            profile_image_192: Sequelize.STRING,
            profile_image_512: Sequelize.STRING,
            profile_team: Sequelize.STRING,
            is_admin: Sequelize.BOOLEAN,
            is_owner: Sequelize.BOOLEAN,
            is_primary_owner: Sequelize.BOOLEAN,
            is_restricted: Sequelize.BOOLEAN,
            is_ultra_restricted: Sequelize.BOOLEAN,
            is_bot: Sequelize.BOOLEAN,
            is_stranger: Sequelize.BOOLEAN,
            updated: Sequelize.DATE,
            is_app_user: Sequelize.BOOLEAN,
            has_2fa: Sequelize.BOOLEAN,
            locale: Sequelize.STRING
        }, {
            indexes: [
                {
                    fields: ["id", "name", "real_name"]
                }
            ]
        });

        this.model = this.models.user;
    }

    public associateModels (): void {
        this.model.hasMany(this.models.message, {foreignKey: "user"});
        this.model.belongsToMany(this.models.channel, {through: "user_channels"});
    }
}