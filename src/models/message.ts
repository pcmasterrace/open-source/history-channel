import * as Sequelize from "sequelize";

export class MessageModel {
    protected sql: Sequelize.Sequelize;
    protected models: Sequelize.ModelsHashInterface;
    public model: Sequelize.Model<any, any>;

    constructor(sql: Sequelize.Sequelize) {
        this.sql = sql;
        this.models = this.sql.models;
        this.defineModel();
    }

    private defineModel(): void {
        this.sql.define("message", {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                primaryKey: true
            },
            time: Sequelize.DATE,
            ts: Sequelize.STRING,
            type: Sequelize.STRING,
            subtype: {
                type: Sequelize.STRING,
                allowNull: true
            },
            text: Sequelize.TEXT,
            channel: {
                type: Sequelize.STRING,
                references: {
                    model: "channels",
                    key: "id"
                }
            },
            user: {
                type: Sequelize.STRING,
                references: {
                    model: "users",
                    key: "id"
                }
            }
        }, {
            indexes: [
                {
                    fields: ["id", "user", "channel", "ts", "time"]
                }
            ]
        });

        this.model = this.models.message;
    }

    public associateModels(): void {
        //this.models.message.belongsTo(this.models.user, {as: "messageId"});
        //this.models.message.belongsTo(this.models.channel, {as: "channelId"});
    }
}